FROM alpine
RUN apk -Uuv add groff less python py-pip
RUN apk add git && apk add wget && apk add gzip && apk add openjdk8-jre
RUN apk --purge -v del py-pip
RUN rm /var/cache/apk/*
RUN cd /tmp/ && wget https://www-eu.apache.org/dist/tomcat/tomcat-9/v9.0.21/bin/apache-tomcat-9.0.21.tar.gz && gunzip apache-tomcat-9.0.21.tar.gz && tar -xf apache-tomcat-9.0.21.tar
ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
COPY GeneradorHTML.py /home/generadorhtml/GeneradorHTML.py
#CMD python /home/generadorhtml/GeneradorHTML.py && /bin/sh
CMD python /home/generadorhtml/GeneradorHTML.py && cp index.html /tmp/apache-tomcat-9.0.21/webapps/ROOT && /tmp/apache-tomcat-9.0.21/bin/catalina.sh run && /bin/sh
#cp /home/generadorhtml/index.html /tmp/apache-tomcat-9.0.21/webapps/ROOT
EXPOSE 8080
EXPOSE 8081
EXPOSE 8082