import socket

def main():
    try: 
        host_name = socket.gethostname() 
        host_ip = socket.gethostbyname(host_name) 
    except: 
        host_name = "<Excepcion>"
        host_ip = "<Excepcion>"


    with open('index.html', 'w') as file_to_write:
        file_to_write.write("<!DOCTYPE HTML>\n")
        file_to_write.write("<html>\n")
        file_to_write.write("<head>\n")
        file_to_write.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n")
        file_to_write.write("        <title>Mi Pagina Corriendo en %s</title>\n" %host_name)
        file_to_write.write("</head>\n")
        file_to_write.write("<body>\n")
        file_to_write.write("        <section>\n")
        file_to_write.write("\n")
        file_to_write.write("           <article>\n")
        file_to_write.write("               <header>\n")
        file_to_write.write("                   <h2>Hello World</h2>\n")
        file_to_write.write("                   <p>Esta es una prueba de docker, estoy corriendo en un contendedor con nombre %s e ip %s</p>\n" %(host_name, host_ip))
        file_to_write.write("               </header>\n")
        file_to_write.write("               <p>Mirame mama soy una pagina web estatica!</p>\n")
        file_to_write.write("           </article>\n")
        file_to_write.write("        </section>\n")
        file_to_write.write("   </br>\n")
        file_to_write.write("        <footer>\n")
        file_to_write.write("           <p>Copyright 2019 Tito</p>\n")
        file_to_write.write("        </footer>\n")
        file_to_write.write("</body>\n")
        file_to_write.write("</html>\n")

                
if __name__ == "__main__":
    main()    